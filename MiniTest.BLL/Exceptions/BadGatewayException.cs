﻿namespace MiniTest.BLL.Exceptions;
    public class BadGatewayException : Exception
    {
        public BadGatewayException(string message) : base(message)
        { }
    }
