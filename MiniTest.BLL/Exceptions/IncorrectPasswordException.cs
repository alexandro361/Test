﻿namespace MiniTest.BLL.Exceptions;
    public class IncorrectPasswordException : BadRequestException
    {
        public IncorrectPasswordException(string message) : base(message)
        { }
    }
