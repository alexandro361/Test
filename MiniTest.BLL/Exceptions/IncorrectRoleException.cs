﻿namespace MiniTest.BLL.Exceptions
{
    public class IncorrectRoleException : BadRequestException
    {
        public IncorrectRoleException(string message) : base(message)
        { }
    }
}
