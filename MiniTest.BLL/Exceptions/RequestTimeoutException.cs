﻿namespace MiniTest.BLL.Exceptions;
    public class RequestTimeoutException: Exception
    {
        public RequestTimeoutException(string message) : base(message)
        { }
    }
