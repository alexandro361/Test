﻿using MiniTest.DAL.Enum;

namespace MiniTest.BLL.Models
{
    public class IdentityModel
    {
        public int? Id { get; set; }
        public Role? Role { get; set; }
    }
}
