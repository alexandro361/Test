﻿namespace MiniTest.BLL.Models;

public class LeadModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Lastname { get; set; }
    public DateTime Birthdate { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string City { get; set; }
    public string Password { get; set; }
    public List<MusicModel> Musics { get; set; }
    public bool IsBanned { get; set; }
}
