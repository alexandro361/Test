﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTest.BLL.Models
{
    public class MusicModel
    {
        public int Id { get; set; }
        public string Song { get; set; }
        public string TextLine { get; set; }
        public LeadModel Lead { get; set; }
        public bool IsDeleted { get; set; }
    }
}
