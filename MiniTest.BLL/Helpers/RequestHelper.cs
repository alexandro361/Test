﻿//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using RestSharp;
//using RestSharp.Authenticators;

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace MiniTest.BLL.Helpers
//{
//    public class RequestHelper
//    {
//        private readonly ILogger<RequestHelper> _logger;
//        private readonly IConfiguration _config;
//        private readonly IRestClient _client;

//        public RequestHelper(ILogger<RequestHelper> logger,
//            IConfiguration config, IRestClient client)
//        {
//            _logger = logger;
//            _config = config;
//            _client = client;
//        }

//        // Senting transaction request (transfer, deposit or withdraw)
//        public async Task<int> SendGetMusicTextBySongNameAsyncRequest<T>(string path, int idsong)
//        {
//            _logger.LogInformation("Try send request to MusicText");
//            // Forming URL link
//            var request = new RestRequest($"https://testwebapi.nicefox.io:32222/GetMusicTextBySongNameAsync", Method.Get);
//            request.AddParameter("songName", idsong); //Add request model
//            //Parsing and checking responce
//            IRestResponse response = await execute(request)).Content;
//            return response;
//        }



//        //Checking response
//        void CheckTransactionError(RestResponse response)
//        {
//            if (response.StatusCode == System.Net.HttpStatusCode.OK)
//                return;
//            if (response.StatusCode == System.Net.HttpStatusCode.RequestTimeout)
//            {
//                _logger.LogError($"Request Timeout {response.ErrorException.Message}");
//                throw new RequestTimeoutException(response.ErrorException.Message);
//            }
//            if (response.StatusCode == System.Net.HttpStatusCode.ServiceUnavailable)
//            {
//                _logger.LogError($"Service Unavailable {response.ErrorException.Message}");
//                throw new ServiceUnavailableException(response.ErrorException.Message);
//            }
//            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
//            {
//                _logger.LogError($"Bad Gatеway {response.ErrorException.Message}");
//                throw new BadGatewayException(response.ErrorException.Message);
//            }
//            if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
//            {
//                _logger.LogError($"Try to login. Incorrected password.");
//                throw new IncorrectPasswordException("Try to login. Incorrected password.");
//            }
//            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
//            {
//                _logger.LogError($"Forbidden {response.ErrorException.Message}");
//                throw new ForbiddenException(response.ErrorException.Message);
//            }
//            if (response.Content == null)
//            {
//                _logger.LogError($"Content equal's null {response.ErrorException.Message}");
//                throw new BadGatewayException(response.ErrorException.Message);
//            }
//            _logger.LogError($"Error Other Service {response.ErrorException.Message}");
//            throw new InternalServerError(response.ErrorException.Message);
//        }

//    }
//}
