﻿using AutoMapper;
using MiniTest.BLL.Models;
using MiniTest.DAL.Entities;

namespace MiniTest.BLL.Configurations;

// Mapping repositories and models from BLL
public class AutoMapperToData : Profile
{
    public AutoMapperToData()
    {
        CreateMap<Lead, LeadModel>().ReverseMap();
        CreateMap<Music, MusicModel>().ReverseMap();
    }
}
