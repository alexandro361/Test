﻿using Microsoft.IdentityModel.Tokens;
using MiniTest.BLL.Exceptions;
using MiniTest.BLL.Helpers;
using MiniTest.BLL.Services.Interfaces;
using MiniTest.DAL.Repositories.Intefaces;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Authentication;
using System.Security.Claims;

namespace MiniTest.BLL.Services
{
    public class AuthorizationsServices : IAuthorizationsServices
    {
        private readonly ILeadRepository _leadRepository;

        public AuthorizationsServices(ILeadRepository leadRepository)
        {
            _leadRepository = leadRepository;
        }

        public string Login(string email, string password)
        {
            var user = _leadRepository.GetByEmail(email);
            if (user == null || user.Result.IsBanned)
                throw new NotFoundException($"User with login {email} was not found.");

            if (!AuthorizationHelper.Verify(password, user.Result.Password))
                throw new AuthenticationException("Password is not correct for this user.");

            var claims = new List<Claim>
        {
            new Claim("Id", user.Id.ToString()),
            new Claim("Name", user.Result.Email),
            new Claim("Role", user.Result.Role.ToString())
        };

            var jwt = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromDays(30)),
                signingCredentials: new SigningCredentials(AuthorizationHelper.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
            );

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}
