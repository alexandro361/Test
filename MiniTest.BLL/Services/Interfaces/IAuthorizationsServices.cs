﻿namespace MiniTest.BLL.Services.Interfaces
{
    public interface IAuthorizationsServices
    {
        public string Login(string email, string password);
    }
}