﻿using MiniTest.BLL.Models;
using MiniTest.DAL.Entities;
using MiniTest.DAL.Enum;

namespace MiniTest.BLL.Services.Interfaces
{
    public interface ILeadService
    {
        Task<int> AddLead(LeadModel leadModel);
        Task ChangePassword(int id, string oldPassword, string newPassword);
        Task ChangeRoleLead(int id, Role role);
        Task<List<LeadModel>> GetAll();
        Task<LeadModel> GetById(int id);
        Task<LeadModel> GetById(int id, IdentityModel leadIdentity);
        Task RestoreById(int id);
        Task UpdateLead(int id, LeadModel leadModel);
    }
}