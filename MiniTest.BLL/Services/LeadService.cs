﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MiniTest.BLL.Exceptions;
using MiniTest.BLL.Helpers;
using MiniTest.BLL.Models;
using MiniTest.BLL.Services.Interfaces;
using MiniTest.DAL.Entities;
using MiniTest.DAL.Enum;
using MiniTest.DAL.Extensions;
using MiniTest.DAL.Repositories.Intefaces;

namespace MiniTest.BLL.Services;

public class LeadService : ILeadService
{
    private readonly ILeadRepository _leadRepository;
    private readonly IMapper _autoMapper;
    private readonly ILogger<LeadService> _logger;
    private readonly IAuthorizationsServices _authorizationsServices;

    public LeadService(ILeadRepository leadRepository, IMapper autoMapper, ILogger<LeadService> logger, IAuthorizationsServices authorizationsServices)
    {
        _leadRepository = leadRepository;
        _autoMapper = autoMapper;
        _logger = logger;
        _authorizationsServices = authorizationsServices;
    }

    public async Task<int> AddLead(LeadModel leadModel)
    {
        _logger.LogInformation("Received a request to create a new lead.");
        // Check in email exists
        var lead = await _leadRepository.GetByEmail(leadModel.Email);
        if (lead != null)
        {
            _logger.LogError($"Try to singup. Email {leadModel.Email.Encryptor()} is already exists.");
            throw new DuplicationException($"Try to singup. Email {leadModel.Email.Encryptor()} is already exists.");
        }
        var mappedLead = _autoMapper.Map<Lead>(leadModel);
        // Hashing password
        mappedLead.Password = AuthorizationHelper.Hash(leadModel.Password);
        var id = await _leadRepository.Add(mappedLead);
        mappedLead.Id = id;


        return id;
    }

    //Updated lead
    public async Task UpdateLead(int id, LeadModel leadModel)
    {
        _logger.LogInformation($"Received a request to update lead with ID = {id}.");
        //Get lead from data base
        var entity = await _leadRepository.GetById(id);
        //Check if lead is null
        ExceptionsHelper.ThrowIfEntityNotFound(id, entity);
        var mappedLead = _autoMapper.Map<Lead>(leadModel);
        await _leadRepository.UpdateById(mappedLead);
    }

    //Deleting lead
    public async Task RestoreById(int id)
    {
        _logger.LogInformation($"Received a request to delete or restore lead with ID =  {id}.");
        var entity = await _leadRepository.GetById(id);
        //Check if lead is null
        ExceptionsHelper.ThrowIfEntityNotFound(id, entity);

            _logger.LogError($"Lead witd ID {entity.Id} is already  banned or restore.");
        

        await _leadRepository.RestoreById(id);
    }

    //Getting all real leads 
    public async Task<List<LeadModel>> GetAll()
    {
        _logger.LogInformation($"Received a request to receive all leads for Auth.");
        //Get all real leads from data base
        var leads = await _leadRepository.GetAll();
        return _autoMapper.Map<List<LeadModel>>(leads);
    }

    // Getting a lead by id
    public async Task<LeadModel> GetById(int id, IdentityModel leadIdentity)
    {
        _logger.LogInformation($"Received to get an lead with an ID {id}.");
        //Check if role is not admin
        if (leadIdentity.Role != Role.Admin)
            // Check if id and id from token aren't same
            ExceptionsHelper.ThrowIfLeadDontHaveAcces(id, (int)leadIdentity.Id);
        //Get lead from data base and check if lead is null
        return await GetById(id);
    }

    //Getting a lead by id
    public async Task<LeadModel> GetById(int id)
    {
        //Get lead from data base
        var entity = await _leadRepository.GetById(id);
        //Check if lead is null
        ExceptionsHelper.ThrowIfEntityNotFound(id, entity);
        return _autoMapper.Map<LeadModel>(entity);
    }

    //Changing password
    public async Task ChangePassword(int id, string oldPassword, string newPassword)
    {
        _logger.LogInformation($"Received a request to change the password of a lead with an ID = {id}.");
        //Get lead from data base
        var entity = await _leadRepository.GetById(id);

        //Check if lead is null
        ExceptionsHelper.ThrowIfEntityNotFound(id, entity);
        //Try authorization with old password. If password is wrong, error will be
        _authorizationsServices.Login(entity.Email, oldPassword);
        // Hashing new password
        string hashPassword = AuthorizationHelper.Hash(newPassword);
        await _leadRepository.ChangePassword(entity.Id, hashPassword);
    }

    //Changing lead's role
    public async Task ChangeRoleLead(int id, Role role)
    {
        _logger.LogInformation($"Received a request to update the role of the lead with ID = {id}.");
        // It is forbidden to change admin's role
        if (role == Role.Admin)
        {
            _logger.LogError($"Authorisation error. The role can be changed to Regular or VIP.");
            throw new IncorrectRoleException("Authorisation error. The role can be changed to Regular or VIP.");
        }
        //Get lead from data base
        var entity = await _leadRepository.GetById(id);
        //Check if lead is null
        ExceptionsHelper.ThrowIfEntityNotFound(id, entity);
        entity.Role = (Role)role;
        await _leadRepository.ChangeRoleLead(entity);
    }
}
