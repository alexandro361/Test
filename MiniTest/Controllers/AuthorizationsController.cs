﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniTest.API.Models;
using MiniTest.BLL.Services.Interfaces;
using MiniTest.DAL.Extensions;
using Swashbuckle.AspNetCore.Annotations;

namespace MiniTest.API.Controllers
{
    [ApiController]
    [Route("api/auth")]
    [AllowAnonymous]
    public class AuthorizationsController : Controller
    {
        private readonly ILogger<AuthorizationsController> _logger;
        private readonly IAuthorizationsServices _authorizationsServices;
        

        public AuthorizationsController(ILogger<AuthorizationsController> logger, IAuthorizationsServices authorizationsServices)
        {
            _logger = logger;
            _authorizationsServices = authorizationsServices;
            

        }

        [HttpPost("login")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [SwaggerOperation("Authentication")]
        public async Task<ActionResult<string>> Login([FromBody] AuthorizationModel auth)
        {
            // We get token from authorization service by RestSharp
            _logger.LogInformation($"Lead with email {auth.Email.Encryptor()} tries to log in.");
            var token = _authorizationsServices.Login(auth.Email, auth.Password);
            _logger.LogInformation($"Lead with email {auth.Email.Encryptor()} successfully logged in.");
            return Ok(token);
        }
    }
}
