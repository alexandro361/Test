﻿using AutoMapper;
using MiniTest.API.Models;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniTest.API.Extensions;
using MiniTest.API.Models;
using MiniTest.BLL.Models;
using MiniTest.BLL.Services.Interfaces;
using MiniTest.DAL.Enum;
using Swashbuckle.AspNetCore.Annotations;

namespace MiniTest.API.Controllers
{
    [ApiController]
    [Route("/api/leads/")]

    public class LeadsController : AdvancedController
    {
        private readonly ILeadService _leadService;
        private readonly IMapper _autoMapper;
        private readonly ILogger<LeadsController> _logger;
        private readonly IValidator<LeadInsertRequest> _validatorLeadInsertRequest;
        private readonly IValidator<LeadUpdateRequest> _validatorLeadUpdateRequest;
        private readonly IValidator<LeadChangePasswordRequest> _validatorLeadChangePasswordRequest;

        public LeadsController(ILeadService leadService, IMapper autoMapper, ILogger<LeadsController> logger, 
            IValidator<LeadInsertRequest> validatorLeadInsertRequest, 
            IValidator<LeadUpdateRequest> validatorLeadUpdateRequest, 
            IValidator<LeadChangePasswordRequest> validatorLeadChangePasswordRequest): base(logger)
        {
            _leadService = leadService;
            _autoMapper = autoMapper;
            _logger = logger;
            _validatorLeadInsertRequest = validatorLeadInsertRequest;
            _validatorLeadUpdateRequest = validatorLeadUpdateRequest;
            _validatorLeadChangePasswordRequest = validatorLeadChangePasswordRequest;
        }




        //Adding lead
        //api/Leads
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(int), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ExceptionResponseModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ExceptionResponseModel), StatusCodes.Status422UnprocessableEntity)]
        [SwaggerOperation("Create lead")]
        public async Task<ActionResult<int>> AddLead([FromBody] LeadInsertRequest leadInsertRequest)
        {
            Validate(leadInsertRequest, _validatorLeadInsertRequest); //Validating data
            _logger.LogInformation($"Received a request to create a new lead.");
            var leadModel = _autoMapper.Map<LeadModel>(leadInsertRequest);
            var idlead = await _leadService.AddLead(leadModel);
            _logger.LogInformation($"Lead successfully created. Received ID = {idlead}");
            return StatusCode(StatusCodes.Status201Created, idlead);
        }

        //Updating lead
        //api/Leads/42
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ExceptionResponseModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ExceptionResponseModel), StatusCodes.Status422UnprocessableEntity)]
        [SwaggerOperation("Update lead by id. Roles: All")]
        public async Task<ActionResult> UpdateLead(int id, [FromBody] LeadUpdateRequest leadUpdateRequest)
        {
            CheckRole(this.GetIdentity(), Role.Admin, Role.Vip, Role.Regular); //Checking role
            Validate(leadUpdateRequest, _validatorLeadUpdateRequest); //Validating data
            _logger.LogInformation($"Received a request to update lead with ID = {id}.");
            var leadModel = _autoMapper.Map<LeadModel>(leadUpdateRequest);
            leadModel.Id = id;
            await _leadService.UpdateLead(id, leadModel);
            _logger.LogInformation($"Lead successfully updated with ID = {id}.");
            return Ok($"Lead successfully updated with ID = {id}.");
        }

        // Changing lead's role
        //api/Leads/42/2
        [HttpPut("{id}/role/{role}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [SwaggerOperation("Change lead's role by id. Roles: Admin")]
        public async Task<ActionResult> ChangeRoleLead(int id, Role role)
        {
            CheckRole(this.GetIdentity(), Role.Admin); //Checking role
            _logger.LogInformation($"Received a request to update the role of the lead with ID = {id}.");
            await _leadService.ChangeRoleLead(id, role);
            _logger.LogInformation($"Successfully updated lead role with ID = {id}.");
            return Ok($"Successfully updated lead role with ID = {id}.");
        }

        // Deleting lead
        //api/Leads/42
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [SwaggerOperation("Delete or Restore lead by id. Roles: Admin")]
        public async Task<ActionResult> DeleteOrRestoreById(int id)
        {
            CheckRole(this.GetIdentity(), Role.Admin);
            _logger.LogInformation($"Received a request to delete or restore lead with ID = {id}.");
            await _leadService.RestoreById(id);
            _logger.LogInformation($"Lead successfully deleted or restore with ID = {id}.");
            return Ok($"Lead successfully deleted or restore with ID = {id}.");
        }


        // Getting all leads
        //api/Leads/
        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(List<LeadResponse>), StatusCodes.Status200OK)]
        [SwaggerOperation("Get all lead. Roles: Admin")]
        public async Task<ActionResult<List<LeadResponse>>> GetAll()
        {

            CheckRole(this.GetIdentity(), Role.Admin); //Checking role
            _logger.LogInformation($"Received a request to receive all leads.");
            var leadModels = await _leadService.GetAll(); // List<LeadModel>
            var outputs = _autoMapper.Map<List<LeadResponse>>(leadModels);
            _logger.LogInformation($"All leads have been successfully received.");
            return Ok(outputs);
        }


        // Getting a lead by Id
        //api/Leads/42
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(LeadResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [SwaggerOperation("Get lead by id. Roles: All")]
        public async Task<ActionResult<LeadResponse>> GetById(int id)
        {
            CheckRole(this.GetIdentity(), Role.Admin, Role.Vip, Role.Regular); //Checking role
            _logger.LogInformation($"Received to get an lead with an ID {id}.");
            var leadModel = await _leadService.GetById(id); // LeadModel
            var output = _autoMapper.Map<LeadResponse>(leadModel);
            _logger.LogInformation($"Successfully received a lead with ID = {id}.");
            return Ok(output);
        }

        // Changing password
        //api/Leads/password
        [HttpPut("password")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ExceptionResponseModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ExceptionResponseModel), StatusCodes.Status422UnprocessableEntity)]
        [SwaggerOperation("Change lead password. Roles: All")]
        public async Task<ActionResult> ChangePassword([FromBody] LeadChangePasswordRequest changePasswordRequest)
        {
            var leadIdentity = this.GetIdentity(); // Checking token
            CheckRole(leadIdentity, Role.Admin, Role.Vip, Role.Regular); //Checking role
            Validate(changePasswordRequest, _validatorLeadChangePasswordRequest); //Validating data
            var id = (int)leadIdentity.Id;
            _logger.LogInformation($"Received a request to change the password of a lead with an ID = {id}.");
            await _leadService.ChangePassword(id, changePasswordRequest.OldPassword, changePasswordRequest.NewPassword);
            _logger.LogInformation($"Successfully changed the password of the lead with ID = {id}.");
            return Ok();
        }

    }
}
