﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using MiniTest.API.Models;
using MiniTest.DAL.Enum;

namespace MiniTest.API.Extensions
{
    public interface IAdvancedController
    {

        void CheckRole(IdentityModel identity, params Role[] roles);
        void Validate<T>(T requestModel, IValidator<T> validator);
    }
}