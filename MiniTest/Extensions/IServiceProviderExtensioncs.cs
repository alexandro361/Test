﻿using Microsoft.AspNetCore.Mvc;
using MiniTest.BLL.Services;
using MiniTest.DAL.Repositories;
using MiniTest.DAL.Repositories.Intefaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog.Extensions.Logging;
using FluentValidation.AspNetCore;
using MiniTest.BLL.Configurations;
using MicroElements.Swashbuckle.FluentValidation.AspNetCore;
using MiniTest.BLL.Services.Interfaces;
using MiniTest.API.Configuration;
using MiniTest.API.Validation;

namespace MiniTest.API.Extensions;

public static class IServiceProviderExtensioncs
{
    // Registration repositories
    public static void RegisterMiniTestRepositories(this IServiceCollection services)
    {
        services.AddScoped<ILeadRepository, LeadRepository>();
    }

    // Registration services
    public static void RegisterMiniTestServices(this IServiceCollection services)
    {
        services.AddScoped<ILeadService, LeadService>();
        services.AddScoped<IAuthorizationsServices, AuthorizationsServices>();

    }

    // Validation
    public static void AddFluentValidation(this IServiceCollection services)
    {
        //Adding FluentValidation
        services.AddFluentValidation(fv =>
        {
            //Register validators  Singleton
            fv.RegisterValidatorsFromAssemblyContaining<LeadInsertRequestValidator>(lifetime: ServiceLifetime.Singleton);
            //Turning off validation with DataAnnotations
            fv.DisableDataAnnotationsValidation = true;
        });
        //Turning off normal validation
        services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });
    }

    //Registration automappers
    public static void RegisterMiniTestAutomappers(this IServiceCollection services)
    {
        services.AddAutoMapper(typeof(AutoMapperFromApi), typeof(AutoMapperToData));
    }

    // Adding authorization
    public static void AddCustomAuth(this IServiceCollection services)
    {
        // Turn off validate token, because we chek in in authorisation service
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                };
            });
        services.AddAuthorization();
    }

    // Settings swagger
    public static void RegisterSwaggerGen(this IServiceCollection services)
    {
        services.AddSwaggerGen(config =>
        {
            config.EnableAnnotations();
            config.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "MyAPI",
                Version = "v1",
                Contact = new OpenApiContact
                {
                    Name = "Git Repository",
                    Url = new Uri("https://github.com/lekss361"),
                }
            });

            config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
            {
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                Description = "JWT Authorization header using the Bearer scheme."

            });
            config.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                          {
                              Reference = new OpenApiReference
                              {
                                  Type = ReferenceType.SecurityScheme,
                                  Id = "Bearer"
                              }
                          },
                         new string[] {}
                    }
                });
        });
        services.AddFluentValidationRulesToSwagger();

    }

    // Adding logger
    public static void RegisterLogger(this IServiceCollection service, IConfiguration config)
    {
        service.Configure<ConsoleLifetimeOptions>(opts => opts.SuppressStatusMessages = true);
        service.AddLogging(loggingBuilder =>
        {
            loggingBuilder.ClearProviders();
            loggingBuilder.SetMinimumLevel(LogLevel.Debug);
            loggingBuilder.AddNLog(config);
        });
    }
}
