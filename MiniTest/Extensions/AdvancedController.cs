﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using MiniTest.API.Models;
using MiniTest.BLL.Exceptions;
using MiniTest.DAL.Enum;
using System.IdentityModel.Tokens.Jwt;

namespace MiniTest.API.Extensions;

public class AdvancedController : Controller
{
    private readonly ILogger<AdvancedController> _logger;


    public AdvancedController(
         ILogger<AdvancedController> logger)
    {
        _logger = logger;
    }

    // Checking role
    // If role is not in the params, will throw exception
    internal void CheckRole(IdentityModel identity, params Role[] roles)
    {
        
        if (identity.Role == null)
        {
            var ex = new ForbiddenException($"Invalid token");
            _logger.LogError(ex.Message);
            throw ex;
        }
        if (!roles.Select(r => r.ToString()).Contains(identity.Role))
        {
            var ex = new ForbiddenException($"Lead id = {identity.Id} doesn't have access to this endpiont");
            _logger.LogError(ex.Message);
            throw ex;
        }
    }

    //Getting info from token
    internal IdentityModel GetIdentity()
    {
        var identity = new IdentityModel();
        var handler = new JwtSecurityTokenHandler();
        string authHeader = Request.Headers["Authorization"];
        if (authHeader == null)
        {
            var ex = new ForbiddenException($"Anonimus doesn't have access to this endpiont");
            _logger.LogError(ex.Message);
            throw ex;
        };
        authHeader = authHeader.Replace("Bearer ", "");
        var jsonToken = handler.ReadToken(authHeader);
        var tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;
        identity.Id = Convert.ToInt32(tokenS.Claims.First(claim => claim.Type == "Id").Value);
        identity.Role = tokenS.Claims.First(claim => claim.Type == "Role").Value;
        return identity;
    }

    // Validating model
    // If data is null or wrong, will throw exception
    internal void Validate<T>(T requestModel, IValidator<T> validator)
    {
        if (requestModel == null)
        {
            var ex = new BadRequestException("You must specify the table details in the request body");
            _logger.LogError(ex.Message);
            throw ex;
        }
        var validationResult = validator.Validate(requestModel);
        if (!validationResult.IsValid)
        {
            var ex = new ValidationException(validationResult.Errors);
            _logger.LogError(ex.Message);
            throw ex;
        }
    }
}
