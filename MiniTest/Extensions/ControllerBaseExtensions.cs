﻿using Microsoft.AspNetCore.Mvc;
using MiniTest.API.Models;
using System.Security.Claims;

namespace MiniTest.API.Extensions;

public static class ControllerBaseExtensions
{
    public static IdentityModel GetIdentity(this ControllerBase controllerBase)
    {
        IdentityModel identityModel = new IdentityModel();
        if (controllerBase.HttpContext.User.Identity is not ClaimsIdentity identity)
            return null;

        if (!int.TryParse(identity.FindFirst(ClaimTypes.Role)?.Value, out var role))
            return null;
        if (!int.TryParse(identity.FindFirst(ClaimTypes.UserData)?.Value, out var userIds))
            return null;

        if (!int.TryParse(identity.FindFirst(ClaimTypes.UserData)?.Value, out var userId))
            return null;
        identityModel.Id = userId;
        identityModel.Role = role.ToString();

        return identityModel;
    }
}