﻿using AutoMapper;
using MiniTest.API.Models;
using MiniTest.BLL.Models;

namespace MiniTest.API.Configuration;

public class AutoMapperFromApi : Profile
{
    public AutoMapperFromApi()
    {
        CreateMap<LeadShortModel, LeadModel>();
        CreateMap<LeadInsertRequest, LeadModel>();
        CreateMap<LeadUpdateRequest, LeadModel>();

        CreateMap<LeadModel, LeadResponse>();

        //CreateMap<TransferShortRequest, TransferRequestModel>();
        //CreateMap<TransactionShortRequest, TransactionRequestModel>();
    }
}