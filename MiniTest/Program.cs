using MiniTest.DAL.Configuration;
using MiniTest.API.Extensions;
using MiniTest.API.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Reading from environment variable
string _logDirectoryVariableName = "LOG_DIRECTORY";
string _connectionStringVariableName = "CRM_CONNECTION_STRING";
string _identityUrlVariableName = "IDENTITY_SERVICE_URL";
string _configsUrlVariableName = "CONFIGS_SERVICE_URL";
string connString = builder.Configuration.GetValue<string>(_connectionStringVariableName);
string logDirectory = builder.Configuration.GetValue<string>(_logDirectoryVariableName);

// connecting data base
builder.Services.Configure<DbConfiguration>(opt =>
{
    opt.ConnectionString = "Host=localhost;Database=MiniTest;Username=postgres;Password=12345";
});

//Setting logging
var config = new ConfigurationBuilder()
           .SetBasePath(logDirectory)
           .AddXmlFile("NLog.config", optional: true, reloadOnChange: true)
           .Build();

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.RegisterSwaggerGen();

builder.Services.RegisterMiniTestRepositories() ;
builder.Services.RegisterMiniTestServices();
builder.Services.RegisterMiniTestAutomappers();
builder.Services.RegisterLogger(config);
builder.Services.AddFluentValidation();

var app = builder.Build();


    app.UseSwagger();
    app.UseSwaggerUI();

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.UseMiddleware<ErrorExceptionMiddleware>();

app.MapControllers();

app.Run();
