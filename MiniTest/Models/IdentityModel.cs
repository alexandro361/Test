﻿using MiniTest.DAL.Enum;

namespace MiniTest.API.Models
{
    public class IdentityModel
    {
        public int? Id { get; set; }
        public string? Role { get; set; }
    }
}
