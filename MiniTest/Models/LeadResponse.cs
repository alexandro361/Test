﻿namespace MiniTest.API.Models
{
    public class LeadResponse:LeadInsertRequest
    {
        public List<Music> Musics { get; set; }
    }
}
