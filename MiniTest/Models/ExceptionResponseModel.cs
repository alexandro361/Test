﻿namespace MiniTest.API.Models
{
    public class ExceptionResponseModel
    {
        public int Code { get; set; }
        public string? Message { get; set; }
    }
}
