﻿using Microsoft.Extensions.Options;
using MiniTest.DAL.Configuration;
using MiniTest.DAL.Entities;
using System.Data;
using Dapper;
using Microsoft.Extensions.Logging;
using MiniTest.DAL.Extensions;
using MiniTest.DAL.Repositories.Intefaces;

namespace MiniTest.DAL.Repositories;

public class LeadRepository : BaseRepository, ILeadRepository
{

    private readonly ILogger<LeadRepository> _logger;
    public LeadRepository(IOptions<DbConfiguration> options, ILogger<LeadRepository> logger) : base(options)
    {
        _logger = logger;
    }

    public async Task<int> Add(Lead lead)
    {
        // Connecting with data base
        using IDbConnection connection = ProvideConnection();
        connection.Open();
        var id = await connection.QueryFirstOrDefaultAsync<int>(
                     $"INSERT INTO lead (Name,LastName,BirthDate,Email,Phone,Password) " +
                     $"VALUES(@Name,@LastName,@BirthDate,@Email,@Phone,@Password) " +
                     $"RETURNING Id",
                     lead);
        return id;
    }

    public async Task UpdateById(Lead lead)
    {
        _logger.LogInformation("Try to connection DB");
        using IDbConnection connection = ProvideConnection();
        _logger.LogInformation("DB connection established successfully.");
        connection.Open();
        await connection.QueryAsync("UPDATE lead SET " +
            $"Name='{lead.Name}'," +
            $"LastName='{lead.Lastname}'," +
            $"BirthDate='{lead.Birthdate}'," +
            $"Phone='{lead.Phone}'" +
            $" WHERE id = '{lead.Id}'");
        _logger.LogInformation($"Lead with id = {lead.Id} updated.");
    }

    public async Task RestoreById(int id)
    {
        _logger.LogInformation("Try to connection DB");
        using IDbConnection connection = ProvideConnection();
        connection.Open();
        await connection.ExecuteAsync($"UPDATE lead SET isbanned= NOT isbanned WHERE Id='{id}'");
    }

    public async Task ChangeRoleLead(Lead lead)
    {
        _logger.LogInformation("Try to connection DB.");
        using IDbConnection connection = ProvideConnection();
        _logger.LogInformation("DB connection established successfully.");

        await connection.ExecuteAsync("UPDATE lead SET role= @Role WHERE Id=@Id", new { Role = lead.Role, Id = lead.Id });
        _logger.LogInformation($"Lead role with id = {lead.Id} updated.");
    }

    public async Task<List<Lead>> GetAll()
    {
        _logger.LogInformation("Try to connection DB");
        using IDbConnection connection = ProvideConnection();
        connection.Open();
        var listLeads = await connection.QueryAsync<Lead>("SELECT * FROM LEAD WHERE isbanned != true");
        return listLeads.ToList();
    }

    public async Task<Lead> GetById(int id)
    {
        _logger.LogInformation("Try to connection DB");
        using IDbConnection connection = ProvideConnection();
        connection.Open();
        var leadbyId = await connection.QueryFirstOrDefaultAsync<Lead>("SELECT * FROM LEAD WHERE Id=@Id", new { Id = id });
        return leadbyId;
    }

    public async Task ChangePassword(int id, string hashPassword)
    {
        _logger.LogInformation("Try to connection DB.");
        using IDbConnection connection = ProvideConnection();
        _logger.LogInformation("DB connection established successfully.");

        await connection.ExecuteAsync("UPDATE FROM LEAD SET password = @password WHERE Id=@Id", new { hashPassword, Id = id });
        _logger.LogInformation($"Lead with ID = {id} changed password");
    }
    public async Task<Lead> GetByEmail(string email)
    {
        _logger.LogInformation("Try to connection DB.");
        using IDbConnection connection = ProvideConnection();
        _logger.LogInformation("DB connection established successfully.");
        connection.Open();
        var leadbyId = await connection.QueryFirstOrDefaultAsync<Lead>("SELECT * FROM LEAD " +
            "WHERE Email=@Email", new { email = email });
        return leadbyId;
    }

}
    