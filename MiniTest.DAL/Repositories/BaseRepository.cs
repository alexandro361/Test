﻿using Microsoft.Extensions.Options;
using MiniTest.DAL.Configuration;
using Npgsql;
using System.Data;

namespace MiniTest.DAL.Repositories;

public class BaseRepository
{
    private string _connectionString;

    public BaseRepository(IOptions<DbConfiguration> options)
    {
        _connectionString = options.Value.ConnectionString;
    }

    protected IDbConnection ProvideConnection() => new NpgsqlConnection(_connectionString);

}
