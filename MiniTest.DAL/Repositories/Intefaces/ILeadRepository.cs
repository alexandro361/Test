﻿using MiniTest.DAL.Entities;

namespace MiniTest.DAL.Repositories.Intefaces
{
    public interface ILeadRepository
    {
        Task<int> Add(Lead lead);
        Task ChangePassword(int id, string hashPassword);
        Task ChangeRoleLead(Lead lead);
        Task<List<Lead>> GetAll();
        Task<Lead> GetByEmail(string email);
        Task<Lead> GetById(int id);
        Task RestoreById(int id);
        Task UpdateById(Lead lead);
    }
}