﻿using Microsoft.Extensions.Options;
using MiniTest.DAL.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTest.DAL.Repositories
{
    internal class MusicRepository : BaseRepository
    {
        public MusicRepository(IOptions<DbConfiguration> options) : base(options)
        {
        }
    }
}
