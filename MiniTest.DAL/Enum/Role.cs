﻿namespace MiniTest.DAL.Enum;

public enum Role : byte
{
    Regular=1,
    Admin,
    Vip
}
