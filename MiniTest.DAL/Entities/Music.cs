﻿namespace MiniTest.DAL.Entities;

public class Music
{
    public int Id { get; set; }
    public string Song { get; set; }
    public string TextLine { get; set; }
    public int LeadId { get; set; }
    public bool IsDeleted { get; set; }

}
