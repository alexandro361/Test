﻿using MiniTest.DAL.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTest.DAL.Entities;

public class Lead
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Lastname { get; set; }
    public DateTime Birthdate { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string City { get; set; }
    public string Password { get; set; }
    public List<Music> Musics { get; set; }
    public Role Role { get; set; }
    public bool IsBanned { get; set; }
}
